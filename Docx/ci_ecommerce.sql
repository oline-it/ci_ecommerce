-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2018 at 11:25 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ci_ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_pass` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_pass`) VALUES
(1, 'Engineer Faruk', 'bdlancer2013@gmail.com', '84ddfb34126fc3a48ee38d7044e87276');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_brand`
--

CREATE TABLE IF NOT EXISTS `tbl_brand` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_brand`
--

INSERT INTO `tbl_brand` (`brand_id`, `brand_name`) VALUES
(1, 'Asus'),
(2, 'HP'),
(3, 'Apple'),
(4, 'DEll'),
(5, 'lenovo'),
(6, 'Mac');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cat_id`, `cat_name`, `publication_status`) VALUES
(7, 'Pizaa', 1),
(8, 'Penne', 1),
(9, 'Bargar', 1),
(10, 'Sandwitch', 1),
(11, 'HotDog', 1),
(12, 'Lasagna', 1),
(13, 'Spaghetti', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE IF NOT EXISTS `tbl_customer` (
  `customer_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`customer_id`, `first_name`, `last_name`, `email`, `password`, `address`, `city`, `postal_code`, `country`) VALUES
(8, 'tanvir', 'faruk', 'admin', '1b0679be72ad976ad5d491ad57a5eec0', 'khulshi', 'chittagong', '4225', 'bangladesh'),
(9, 'tanvir', 'kjj', 'admin', '1b0679be72ad976ad5d491ad57a5eec0', 'jjj', 'jj', '', ''),
(10, 'tanvir', 'kjj', 'admin', '1b0679be72ad976ad5d491ad57a5eec0', 'jjj', 'jj', '', ''),
(11, 'tanvir', 'kjj', 'admin', '1b0679be72ad976ad5d491ad57a5eec0', 'jjj', 'jj', '', ''),
(12, '', '', 'admin', '1b0679be72ad976ad5d491ad57a5eec0', '', '', '', ''),
(13, '', '', 'admin', '1b0679be72ad976ad5d491ad57a5eec0', '', '', '', ''),
(14, '', '', 'admin', '1b0679be72ad976ad5d491ad57a5eec0', '', '', '', ''),
(15, 'tanvir', 'faruk', 'admin', '1b0679be72ad976ad5d491ad57a5eec0', '', '', '', ''),
(16, 'MD ', 'faruk', 'bdlancer2013@gmail.com', '1b0679be72ad976ad5d491ad57a5eec0', 'khulshi', 'ctg', '42225', 'bangladesh'),
(17, 'tanvir', 'faruk', 'admin', '1b0679be72ad976ad5d491ad57a5eec0', '', '', '', ''),
(18, 'tanvir', 'hh', 'admin', '1b0679be72ad976ad5d491ad57a5eec0', '', '', '', ''),
(19, 'ggg', 'ggghg', 'admin', '1b0679be72ad976ad5d491ad57a5eec0', '', '', '', ''),
(21, 'tanvir', 'faruk', 'admin', '1b0679be72ad976ad5d491ad57a5eec0', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE IF NOT EXISTS `tbl_order` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `order_total` float NOT NULL,
  `order_status` varchar(255) NOT NULL DEFAULT 'pending',
  `order_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`order_id`, `customer_id`, `shipping_id`, `payment_id`, `order_total`, `order_status`, `order_time`) VALUES
(6, 19, 8, 15, 2420, 'pending', '2018-02-07 09:44:39'),
(7, 19, 8, 16, 2420, 'pending', '2018-02-07 09:44:54'),
(8, 19, 8, 17, 2750, 'pending', '2018-02-07 10:03:55'),
(9, 20, 10, 18, 4950, 'pending', '2018-02-07 10:15:53'),
(10, 21, 11, 19, 5170, 'pending', '2018-02-07 11:39:08');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_details`
--

CREATE TABLE IF NOT EXISTS `tbl_order_details` (
  `details_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `pro_name` varchar(255) NOT NULL,
  `pro_price` float NOT NULL,
  `pro_qty` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order_details`
--

INSERT INTO `tbl_order_details` (`details_id`, `order_id`, `pro_id`, `pro_name`, `pro_price`, `pro_qty`) VALUES
(4, 6, 13, 'Pizza Delicioso', 200, 1),
(5, 6, 14, 'VERY LONG PRODUCT NAME GOES HERE', 200, 10),
(6, 7, 13, 'Pizza Delicioso', 200, 1),
(7, 7, 14, 'VERY LONG PRODUCT NAME GOES HERE', 200, 10),
(8, 8, 13, 'Pizza Delicioso', 200, 1),
(9, 8, 14, 'VERY LONG PRODUCT NAME GOES HERE', 200, 10),
(10, 8, 16, 'Birani food', 300, 1),
(11, 9, 13, 'Pizza Delicioso', 200, 11),
(12, 9, 14, 'VERY LONG PRODUCT NAME GOES HERE', 200, 10),
(13, 9, 16, 'Birani food', 300, 1),
(14, 10, 13, 'Pizza Delicioso', 200, 12),
(15, 10, 14, 'VERY LONG PRODUCT NAME GOES HERE', 200, 10),
(16, 10, 16, 'Birani food', 300, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE IF NOT EXISTS `tbl_payment` (
  `payment_id` int(11) NOT NULL,
  `payment_method` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`payment_id`, `payment_method`) VALUES
(1, 'bkash'),
(2, 'bkash'),
(3, 'bkash'),
(4, 'bkash'),
(5, 'bkash'),
(6, 'cod'),
(7, 'cod'),
(8, 'cod'),
(9, 'cod'),
(10, 'bkash'),
(11, 'bkash'),
(12, 'bkash'),
(13, 'cod'),
(14, 'cod'),
(15, 'bkash'),
(16, 'bkash'),
(17, 'bkash'),
(18, 'cod'),
(19, 'cod');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE IF NOT EXISTS `tbl_product` (
  `pro_id` int(11) NOT NULL,
  `pro_name` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `pro_description` varchar(255) NOT NULL,
  `pro_image` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `new_price` int(11) NOT NULL,
  `featured` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`pro_id`, `pro_name`, `cat_id`, `brand_id`, `pro_description`, `pro_image`, `qty`, `price`, `new_price`, `featured`) VALUES
(7, 'Pizza Delicioso', 7, 1, '<span xss=removed>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specim', 'uploads/pizza.jpg', 10, 15, 20, '1'),
(8, 'Pizza Delicioso', 7, 1, '<h2 class="heading-title" xss=removed><span xss=removed>Pizza Delicioso</span></h2>  ', 'uploads/prod_pic21.jpg', 200, 200, 300, '0'),
(9, 'Delicioso Pizza ', 7, 1, '<span xss=removed>rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen', 'uploads/pizza2.jpg', 5, 250, 200, '1'),
(10, 'Pizza Delicioso', 7, 1, '<span xss=removed>rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen', 'uploads/pizza3.jpg', 100, 200, 200, '1'),
(11, 'Pizza Delicioso', 7, 1, '  ', 'uploads/pizza4.jpg', 100, 100, 200, '1'),
(12, 'Pizza Delicioso', 7, 1, '  ', 'uploads/pizza5.jpg', 1, 200, 200, '1'),
(13, 'Pizza Delicioso', 7, 1, '<span xss=removed>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specim', 'uploads/prod_pic22.jpg', 100, 200, 200, '0'),
(14, 'VERY LONG PRODUCT NAME GOES HERE', 8, 1, '<span xss=removed>orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</span>  ', 'uploads/prod_pic4.jpg', 100, 200, 200, '0'),
(15, 'VERY LONG PRODUCT NAME GOES HERE', 9, 1, '<span xss=removed>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specim', 'uploads/prod_pic8.jpg', 1, 200, 20, '0'),
(16, 'Birani food', 8, 1, '<span xss=removed>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specim', 'uploads/prod_pic3.jpg', 100, 15, 300, '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipping`
--

CREATE TABLE IF NOT EXISTS `tbl_shipping` (
  `shipping_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_shipping`
--

INSERT INTO `tbl_shipping` (`shipping_id`, `full_name`, `mobile`, `address`, `city`, `postal_code`, `country`) VALUES
(9, 'MD Faruk', '0193287412', 'south khulshi', 'chittagong', '4225', 'bangladesh'),
(10, 'md faruk', '01932870746', 'south khulshi', 'chittagong', '4225', 'bangladesh'),
(11, 'tanvir', '01932870746', 'khulshi', 'chittagong', '4225', 'bangladesh');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  ADD PRIMARY KEY (`details_id`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  ADD PRIMARY KEY (`shipping_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  MODIFY `details_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `pro_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  MODIFY `shipping_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
