<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
     public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id!=NULL){
            redirect('super_admin');
        }
    }
    
    
    

	
		public function index()
	{
		
		$this->load->view('admin/admin_login');
		
	}
	
public function admin_login_check()
	{
            //get the form data
          $admin_email=$this->input->post('admin_email',true);
          $admin_pass=$this->input->post('admin_pass',true);
          
          $result= $this->admin_model->admin_login_check_info($admin_email,$admin_pass);
          
        $sdata=array();
          if($result){
              $data=array();
              $sdata['admin_name']=$result->admin_name;
              $sdata['admin_id']=$result->admin_id;
              $this->session->set_userdata($sdata);
              redirect('super_admin');
          }
         else{
             $sdata['exception']='your login details not correct';
              $this->session->set_userdata($sdata);
              redirect('admin');
         }
          
   
	}
	
	
}  //end class
