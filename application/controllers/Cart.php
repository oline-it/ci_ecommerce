<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cart extends CI_Controller {
   
 
    public function add_to_cart(){
        $qty= $this->input->post('quantity',true);
        $pro_id= $this->input->post('pro_id',true);
        $product_info= $this->welcome_model->select_single_product_id($pro_id);
       
        $data=array(
            'id'=>$product_info->pro_id,
            'qty'=>$qty,
            'price'=>$product_info->new_price,
            'name'=>$product_info->pro_name,
            'image'=>$product_info->pro_image
            
            
        );  
        $this->cart->insert($data);
        redirect('Cart/view_cart');
    }
    
    public function view_cart(){
        $data=array();
        $data['all_published_category']= $this->welcome_model->select_all_published_category();
        $data['featured']=$this->welcome_model->select_all_featured();
        $data['maincontent']= $this->load->view('view_cart',$data,true);
        $this->load->view('master',$data);
    }
    public function update_cart(){
        $qty= $this->input->post('qty');
        $rowid= $this->input->post('rowid');
        $data=array(
         'rowid' =>$rowid,
          'qty'=>$qty
            
        );
        $this->cart->update($data);  
       redirect('Cart/view_cart');
    }
    
    
    public  function delete_cart($rowid){
        
        $data=array(
         'rowid' =>$rowid,
          'qty'=>0  
        );
       $this->cart->update($data);  
       redirect('Cart/view_cart');
    }
}//end class

?>