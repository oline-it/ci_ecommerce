<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_admin extends CI_Controller {

      public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==NULL){
            redirect('admin');
        }
    }
	
	public function index()
	{
	
	$data=array();
        $data['admin_maincontent']=$this->load->view('admin/dashboard','',true);
        $this->load->view('admin/admin_master',$data);
	}
	
	
	public function logout(){
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('admin_name');
        
        $sdata=array();
        $sdata['message']='you have done logout';
        $this->session->set_userdata($sdata);
        redirect('admin');
        
        
    }
	 //add category form 
    public function add_category(){
        $data=array();
        $data['admin_maincontent']=$this->load->view('admin/add_category','',true);
        $this->load->view('admin/admin_master',$data); 
    }
    
    public function save_category(){
        $data=array();
        $data['cat_name']=$this->input->post('cat_name',true);
        $data['publication_status']=$this->input->post('publication_status',true);
        $this->super_admin_model->save_category_info($data);
        $sdata=array();
        $sdata['message']='category added successfully';
        $this->session->set_userdata($sdata);
                
        redirect('super_admin/add_category');
}


    public function manage_category(){
        
        $data=array();
        $data['all_published_category']= $this->super_admin_model->select_all_published_category();
        $data['admin_maincontent']=$this->load->view('admin/manage_category',$data,true);
        $this->load->view('admin/admin_master',$data); 
    }
    
    
    
      public function edit_category($cat_id){
        
        $data=array();
        $data['cat_id']=$this->super_admin_model->select_cat_by_id($cat_id);
        $data['admin_maincontent']=$this->load->view('admin/edit_category',$data,true);
        $this->load->view('admin/admin_master',$data); 
    }
    
    //update category 
    
    public function update_category(){    
        $data=array();
        $cat_id=$this->input->post('cat_id',true);
        $data['cat_name']=$this->input->post('cat_name',true);
        $data['publication_status']=$this->input->post('publication_status',true);
        $this->super_admin_model->update_category_info($cat_id,$data);         
        redirect('super_admin/manage_category');
    }
    
     public function delete_category($cat_id){
         $data=array();
         $data['cat_id']=$this->super_admin_model->delete_category_by_id($cat_id);
         redirect('super_admin/manage_category');
     }
     
     
   
    public function add_brand(){
        $data=array();
        $data['admin_maincontent']=$this->load->view('admin/add_brand','',true);
        $this->load->view('admin/admin_master',$data); 
    }
     
        public function save_brand(){
        $data=array();
        $data['brand_name']=$this->input->post('brand_name',true);
        
        $this->super_admin_model->save_brand_info($data);
        $sdata=array();
        $sdata['message']='Brand added successfully';
        $this->session->set_userdata($sdata);
                
        redirect('super_admin/add_brand');
}

     
     
     
     
     
     
     
      public function manage_brand(){
        
        $data=array();
        $data['all_brand']= $this->super_admin_model->select_all_brand();
        $data['admin_maincontent']=$this->load->view('admin/manage_brand',$data,true);
        $this->load->view('admin/admin_master',$data); 
    }
     
     
    
  
    public function add_product(){
        $data=array();
		$data['all_category']=$this->super_admin_model->select_all_published_category();
		$data['all_brand']=$this->super_admin_model->select_all_brand();
        $data['admin_maincontent']=$this->load->view('admin/add_product',$data,true);
        $this->load->view('admin/admin_master',$data); 
    }
    
	  public function save_product(){
        $data=array();
        $data['pro_name']=$this->input->post('pro_name',true);
        $data['cat_id']=$this->input->post('cat_id',true);
        $data['brand_id']=$this->input->post('brand_id',true);
		$data['pro_description']=$this->input->post('pro_description',true);
		$data['qty']=$this->input->post('qty',true);
		$data['price']=$this->input->post('price',true);
		$data['new_price']=$this->input->post('new_price',true);
		$data['featured']=$this->input->post('featured',true);
		if($data['featured']==on){
			$data['featured']=1;	
		}
		else{		
	$data['featured']=0;
		}
		
		 //start upload image
	            $fdata=array();
		        $error='';
                $config['upload_path']          = 'uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 1500;
                $config['max_width']            = 1500;
                $config['max_height']           = 1000;

                $this->load->library('upload', $config);
                if(!$this->upload->do_upload('pro_image')){
					
					$error=$this->upload->display_errors();
                    
					exit();
                    
                }
                else{
                    $fdata=$this->upload->data();
					$data['pro_image']= $config['upload_path'].$fdata['file_name'];
                }
				
				//end uplaod image
		
        
        $this->super_admin_model->save_product_info($data);
        $sdata=array();
        $sdata['message']='product added successfully';
        $this->session->set_userdata($sdata);               
        redirect('super_admin/add_product');
}
	
    
    public function manage_product(){
        
        $data=array();
        $data['all_product']=$this->super_admin_model->select_all_product();
        $data['admin_maincontent']=$this->load->view('admin/manage_product',$data,true);
        $this->load->view('admin/admin_master',$data); 
    }
    
    public function delete_product($pro_id){
        
        $this->super_admin_model->select_delete_product($pro_id);
        redirect('super_admin/manage_product');
    }
}  //end class
