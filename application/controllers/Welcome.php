<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {	
	public function index()
	{
		$data=array();
		$data['slider']=true;
		$data['all_published_category']= $this->welcome_model->select_all_published_category();
		$data['all_product']=$this->welcome_model->select_all_product();
		$data['featured']=$this->welcome_model->select_all_featured();
		$data['maincontent']=$this->load->view('home_content',$data,true);
		$this->load->view('master',$data);
	}
	
	public function about_us(){
		$data=array();
		$data['all_published_category']= $this->welcome_model->select_all_published_category();
		$data['featured']=$this->welcome_model->select_all_featured();
		$data['maincontent']=$this->load->view('about','',true);
		$this->load->view('master',$data);
		
	}
	
	
		public function single_product($pro_id)
	{
		$data=array();
		
		$data['all_published_category']= $this->welcome_model->select_all_published_category();
		$data['featured']=$this->welcome_model->select_all_featured();
		$data['single_product']= $this->welcome_model->select_single_product_id($pro_id);
		$data['maincontent']=$this->load->view('single_product',$data,true);
		$this->load->view('master',$data);
	}
	
	
	public  function productbycat($cat_id){
            $data=array();
            $data['slider']=true;
            $data['all_published_category']= $this->welcome_model->select_all_published_category();
	    $data['featured']=$this->welcome_model->select_all_featured();
            $data['product_by_cat']=$this->welcome_model->select_productbycat($cat_id);
            $data['maincontent']=$this->load->view('productbycat',$data,true);
            $this->load->view('master',$data);
        }
	
}  //end class
