<?php

class Super_admin_Model extends CI_Model{
    
     public function save_category_info($data){
        $this->db->insert('tbl_category',$data);
        
    } 
    
    
    public function select_all_published_category(){
        $this->db->select('*');
        $this->db->from('tbl_category');
       
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    
    
     
    public function select_cat_by_id($cat_id){
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where('cat_id',$cat_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
      public function update_category_info($cat_id,$data){
        $this->db->where('cat_id',$cat_id);
        $this->db->update('tbl_category',$data);
        
    }
    
     public function delete_category_by_id($cat_id){
        $this->db->where('cat_id',$cat_id);
        $this->db->delete('tbl_category');
        
    }
    
    
       public function save_brand_info($data){
        $this->db->insert('tbl_brand',$data);
        
    }
    
    
    
    
       public function select_all_brand(){
        $this->db->select('*');
        $this->db->from('tbl_brand');
       
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    
    
    
	
      public function save_product_info($data){
        $this->db->insert('tbl_product',$data);
        
    } 
    
    
       public function select_all_product(){
        $this->db->select('*');
        $this->db->from('tbl_product');
       
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    
    public function select_delete_product($pro_id){
        $this->db->where('pro_id',$pro_id);
        $this->db->delete('tbl_product');
        
    }
    
    
    
    
    
    
    
}

