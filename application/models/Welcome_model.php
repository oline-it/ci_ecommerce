<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_model extends CI_Model {
	 
    public function select_all_published_category(){
        $this->db->select('*');
        $this->db->from('tbl_category');
       
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
   
    
     public function select_all_product(){
        $this->db->select('*');
        $this->db->from('tbl_product');
       $this->db->where('featured',0);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
	
	
	     public function select_all_featured(){
        $this->db->select('*');
        $this->db->from('tbl_product');
        $this->db->where('featured',1);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    
	public function select_single_product_id($pro_id){
	$this->db->select('*');
        $this->db->from('tbl_product');
        $this->db->where('pro_id',$pro_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
		
		
	}
        
        
        
        public function select_productbycat($cat_id){
	$this->db->select('*');
        $this->db->from('tbl_product');
        $this->db->where('cat_id',$cat_id);
         $this->db->where('featured',0);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
		
		
	}
	
}//end class 
?>
