<div class="box span8">
					<div class="box-header" data-original-title="">
                                            <h2 style="color:green;">
                                                
       <?php
    $msg=$this->session->userdata('message');
    if($msg){
       echo $msg;
       $this->session->unset_userdata('message');
        
    }
    
    
    ?>
                                                
                                            </h2>
						
					</div>
					<div class="box-content">
						<form class="form-horizontal" action="<?php echo base_url();?>super_admin/save_product" method="POST" enctype="multipart/form-data">
						  <fieldset>
							<div class="control-group">
							  <label class="control-label" for="typeahead">Product Name </label>
							  <div class="controls">
								<input type="text" name="pro_name">
							  </div>
							</div>
							
                              

							       <div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton">select category </label>
  <div class="col-md-4">
      <select name="cat_id">
          <?php foreach($all_category as $v_category){?>
		       
			   <option value="<?php echo $v_category->cat_id;?>"> <?php echo $v_category->cat_name;?> </option>
			  
		<?php 
		}
		?>
      </select>
  </div>
</div>  
<br> 


							       <div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton">Select Brand </label>
  <div class="col-md-4">
      <select name="brand_id">
          <?php foreach($all_brand as $v_brand){?>
		      
			   <option value="<?php echo $v_brand->brand_id;?>"> <?php echo $v_brand->brand_name;?> </option>
			  
		<?php 
		}
		?>
         
      </select>
  </div>
</div> 
<br>



                             <div class="control-group hidden-phone">
							  <label class="control-label" for="textarea2">Product Description </label>
							  <div class="controls">
								<div class="cleditorMain" style="width: 500px; height: 250px;">
								
								
								
								<textarea class="cleditor" name="pro_description" id="textarea2" rows="3" style="display: none; width: 700px; height: 197px;">  </textarea>
								<iframe frameborder="0" src="javascript:true;" style="width: 500px; height: 197px;"></iframe></div>
							  </div>
							</div>
							
							
							<div class="control-group">
							  <label class="control-label" for="typeahead">Product Image </label>
							  <div class="controls">
								<input type="file" name="pro_image">
							  </div>
							</div>
							
							
							<div class="control-group">
							  <label class="control-label" for="typeahead">Product Quantity</label>
							  <div class="controls">
								<input type="text" name="qty">
							  </div>
							</div>
							
							<div class="control-group">
							  <label class="control-label" for="typeahead">Product price</label>
							  <div class="controls">
								<input type="text" name="price">
							  </div>
							</div>
							
							
							<div class="control-group">
							  <label class="control-label" for="typeahead">Product new price</label>
							  <div class="controls">
								<input type="text" name="new_price">
							  </div>
							</div>
							
							<div class="control-group">
							  <label class="control-label" for="typeahead">Fetured Slider Image</label>
							  <div class="controls">
								<input type="checkbox" name="featured">
							  </div>
							</div>
							
							
							
							<div class="form-actions">
							  <button type="submit" class="btn btn-primary">save</button>
							  <button type="reset" class="btn">Cancel</button>
							</div>
						  </fieldset>
						</form>   

					</div>
				</div>