<div class="box span8">
					<div class="box-header" data-original-title="">
						<h2><i class="halflings-icon user"></i><span class="break"></span>Manage Brand</h2>
						
					</div>
					<div class="box-content">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
					
						
						<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
						  <thead>
							  <tr role="row"><th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 167px;">Brand ID</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 241px;">Brand Name</th>
							  
							
							
							  
							  
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 298px;">Actions</th></tr>
						  </thead>   
						 
					  <tbody role="alert" aria-live="polite" aria-relevant="all">
                                               <?php
                                                  foreach($all_brand as $v_brand){?>
                                                      
                                                  
                                              <tr class="odd">
								<td class="  sorting_1"><?php echo $v_brand->brand_id;?></td>
								<td class="center "><?php echo $v_brand->brand_name;?></td>
					
                                                                                     
                                                                
								<td class="center ">
									
									<a class="btn btn-info" href="<?php echo $v_brand->brand_id;?>">
										<i class="halflings-icon white edit"></i>  
									</a>
									<a class="btn btn-danger" href="<?php echo $v_brand->brand_id;?>">
										<i class="halflings-icon white trash"></i> 
									</a>
								</td>
							</tr>
												  <?php
												  }?>                         
                                                      
							</tbody></table>
							
							</div>            
					</div>
				</div>
