<div class="box span8">
					<div class="box-header" data-original-title="">
						<h2><i class="halflings-icon user"></i><span class="break"></span>Manage Category</h2>
						
					</div>
					<div class="box-content">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
					
						
						<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
						  <thead>
							  <tr role="row"><th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 167px;">Category ID</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 241px;">Category Name</th>
							  
							
							  <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 150px;"> Publication Status</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 298px;">Actions</th></tr>
						  </thead>   
						 
					  <tbody role="alert" aria-live="polite" aria-relevant="all">
                                               <?php
                                                  foreach($all_published_category as $v_category){?>
                                                      
                                                  
                                              <tr class="odd">
								<td class="  sorting_1"><?php echo $v_category->cat_id;?></td>
								<td class="center "><?php echo $v_category->cat_name;?></td>
						<?php
                                                if($v_category->publication_status==1){?>
                                                    
                                                  <td class="center ">
									<span class="label label-success">Active</span>
								</td>  
                                                <?php
                                                
                                                }  else{?>
                                                    
                                                    <td class="center ">
									<span class="label label-danger">Inactive</span>
								</td>  
                                                <?php 
                                                
                                                }
                                                
                                                ?>
								
                                                                
                                                                
                                                                
								<td class="center ">
									<a class="btn btn-success" href="<?php echo base_url();?>super_admin/unpublihed_category/">
										<i class="halflings-icon white zoom-in"></i>  
									</a>
									<a class="btn btn-info" href="<?php echo base_url();?>super_admin/edit_category/<?php echo $v_category->cat_id;?>">
										<i class="halflings-icon white edit"></i>  
									</a>
									<a class="btn btn-danger" href="<?php echo base_url();?>super_admin/delete_category/<?php echo $v_category->cat_id;?>">
										<i class="halflings-icon white trash"></i> 
									</a>
								</td>
							</tr>
                                                        
                          <?php
						  }
                                                  
                         ?>
                                                        
							</tbody></table>
							
							</div>            
					</div>
				</div>
