<!DOCTYPE html>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Spicylicious - Premium HTML template by D.Koev</title>
<link rel="stylesheet" href="<?php echo base_url();?>/assets/site/css/stylesheet.css" type="text/css" media="screen" />
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/site/css/jquery-ui-1.8.9.custom.css" />
<!-- jQuery and Custom scripts -->
<script src="<?php echo base_url();?>/assets/site/js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>/assets/site/js/jquery.cycle.lite.1.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>/assets/site/js/custom_scripts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>/assets/site/js/jquery.roundabout.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/site/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/site/js/tabs.js"></script>
<!-- Tipsy -->
<script src="<?php echo base_url();?>/assets/site/js/tipsy/jquery.tipsy.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>/assets/site/js/tipsy/css.tipsy.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>/assets/site/js/jquery.tweet.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>/assets/site/js/jquery.tweet.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!-- MAIN WRAPPER -->
<div id="container"> 
  
  <!-- SIDEFEATURES -->

  
  <!-- HEADER -->
  <div id="header">
    <div class="inner">
      <ul class="main_menu menu_left">
        <li><a href="#">My Account</a></li>
        <li><a href="#">Wish List <b>(3)</b></a></li>
        <li><a href="<?php echo base_url();?>welcome/about_us">About Us</a></li>
        <li class="warning"><a href="<?php echo base_url();?>welcome/index">Home</a>
          
        </li>
      </ul>
      <div id="logo"><a href="<?php echo base_url();?>welcome/index"><img src="<?php echo base_url();?>/assets/site/image/logo.png" width="217" height="141" alt="Spicylicious store" /></a></div>
      <ul class="main_menu menu_right">
        <li><a href="#">Compare</a></li>
        <li><a href="<?php echo base_url();?>cart/view_cart">Shopping Cart(<?php echo $this->cart->total_items();?>)</a></li>
        <li><a href="#">Checkout</a></li>
        <li><a href="#">Contact Us</a></li>
      </ul>
      <div id="welcome"> Welcome visitor you can <a href="#">login</a> or <a href="#">create an account</a>. </div>
      <div class="menu">
        <ul id="topnav">
		<?php
		foreach($all_published_category as $v_category){?>
			
			 <li><a href="<?php echo base_url();?>welcome/productbycat/<?php echo $v_category->cat_id;?>"><?php echo $v_category->cat_name;?></a>
			
		<?php
		}
		?>        
        </ul>
      </div>
    </div>
  </div>
  <!-- END OF HEADER --> 
 
  <div id="content_holder" class="fixed">
    <div class="inner">
      <div id="content">
          <?php  if(isset($slider)){?>
        <div class="box featured-box">
          <h2 class="heading-title"><span>Featured Products</span></h2>
          <div class="box-content">
            <ul id="myRoundabout">
		
		<?php foreach($featured as $v_featured){?>
			<li>
                <div class="prod_holder"> <a href="<?php echo base_url();?>welcome/single_product/<?php echo $v_featured->pro_id;?>"> <img src="<?php echo base_url().$v_featured->pro_image;?>" alt="Spicylicious store" /> </a>
                  <h3><?php echo $v_featured->pro_name;?></h3>
                </div>
                <span class="pricetag"><?php echo $v_featured->price;?></span> </li>
			
		<?php 
		}
     
              ?>		
             
            </ul>
            <a href="#" class="previous_round">Previous</a> <a href="#" class="next_round">Next</a> </div>
        </div>
          
      
          
        <div class="box">
          <h2 class="heading-title"><span>Welcome to Spicylicious</span></h2>
          <div class="box-content">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
          </div>
        </div>
       
             <?php  }?>
        <div class="box">
          
          <div class="box-content">
            <?php echo $maincontent;?>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END OF CONTENT --> 
  
  <!-- PRE-FOOTER -->
  <div id="pre_footer">
    <div class="inner">

    </div>
  </div>
  <!-- END OF PRE-FOOTER --> 
  
  <!-- FOOTER -->
  <div id="footer">
    <div class="inner">
      <div class="column_big"> 
        <!-- FOOTER MODULES AREA -->
        <div class="footer_modules">
          <h3>About spicylicious</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
        </div>
        <!-- END OF FOOTER MODULES AREA -->
        <div class="footer_social">
          <h3>Spread the word</h3>
          <!-- AddThis Button BEGIN -->
          <div class="addthis_toolbox addthis_default_style "><script type="text/javascript">
      //<![CDATA[
            document.write('<a class="addthis_button_google_plusone" g:plusone:size="medium"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>');
      //]]>
     </script> 
          </div>
          <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e7280075406aa87"></script> 
          <!-- AddThis Button END --> 
        </div>
      </div>
      <div class="column_small">
        <h3>Customer Service</h3>
        <ul>
          <li><a href="#">Home</a></li>
          <li><a href="#">My Account</a></li>
          <li><a href="#">Order History</a></li>
          <li><a href="#">Wishlist</a></li>
          <li><a href="#">Newsletter</a></li>
          <li><a href="#">Returns</a></li>
        </ul>
      </div>
      <div class="column_small">
        <h3>Information</h3>
        <ul>
          <li><a href="about.html">About Us</a></li>
          <li><a href="#">Delivery Information</a></li>
          <li><a href="#">Privacy policy</a></li>
          <li><a href="#">Terms and conditions</a></li>
          <li><a href="contact.html">Contact Us</a></li>
          <li><a href="sitemap.html">site map</a></li>
        </ul>
      </div>
      <div class="column_small">
        <h3>Extras</h3>
        <ul>
          <li><a href="#">Brands</a></li>
          <li><a href="#">Gift vouchers</a></li>
          <li><a href="#">Affiliates</a></li>
          <li><a href="#">Specials</a></li>
        </ul>
      </div>
      <div class="clear"></div>
      <span class="copyright">Spicylicious theme by <a href="http://themeforest.net/user/Koev/portfolio?ref=Koev">Dimitar Koev - theAlThemist</a>. </span> </div>
  </div>
  <!-- END OF FOOTER --> 
  
</div>
<!-- END OF MAIN WRAPPER --> 
<script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script> 
<script type="text/javascript" src="http://twitter.com/statuses/user_timeline/d_koev.json?callback=twitterCallback2&amp;count=3"></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
$('#twitter_update_list').cycle({
		fx: 'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
		next:   '#tweet_next', 
    	prev:   '#tweet_prev'
		}); 
		});
//--></script> 
<script type="text/javascript">
   $(document).ready(function() {
		var interval;
		$('ul#myRoundabout')
		.roundabout({
		  	'btnNext': '.next_round',
			'btnPrev': '.previous_round' 
		  }
		  )
		.hover(
		function() {

		clearInterval(interval);
		},
		function() {

		interval = startAutoPlay();
		});

		interval = startAutoPlay();
		});
		function startAutoPlay() {
		return setInterval(function() {
		$('ul#myRoundabout').roundabout_animateToPreviousChild();
		}, 3000);
	} 
</script>
</body>
</html>
