<div class="box-product fixed">

<?php
foreach($product_by_cat as $v_product){?>
	
	<div class="prod_hold"> <a class="wrap_link" href="<?php echo base_url();?>welcome/single_product/<?php echo $v_product->pro_id;?>"> <span class="image"><img src="<?php echo base_url().$v_product->pro_image;?>" alt="Spicylicious store" /></span> </a>
                <div class="pricetag_small"> <span class="old_price"><?php echo $v_product->price;?></span> <span class="new_price"><?php echo $v_product->new_price;?></span> </div>
                <div class="info">
                  <h3><?php echo $v_product->pro_name;?></h3>
                  <p><?php echo $v_product->pro_description;?>.</p>
                  <a class="add_to_cart_small" href="#">Add to cart</a> <a class="wishlist_small" href="#">Wishlist</a> <a class="compare_small" href="#">Compare</a> </div>
              </div>
<?php	
}
?>
              
			  
    </div>