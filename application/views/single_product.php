<div id="content_holder" class="fixed">
    <div class="inner">

	
      <div class="breadcrumb"> <a href="index.html" tppabs="http://metagraphics.eu/spicy/index.html">Home</a> » <a href="category.html" tppabs="http://metagraphics.eu/spicy/category.html">Pizza</a> » Pizza Delicioso </div>
      <h2 class="heading-title"><span><?php echo $single_product->pro_name;?></span></h2>
      
      <!-- PRODUCT INFO -->
      <div class="product-info fixed">
        <div class="left">
          <div class="image"> <a href="bigimage00.jpg" tppabs="http://metagraphics.eu/spicy/image/bigimage00.jpg" class="cloud-zoom" id="zoom1" rel="adjustX: 5, adjustY:0, zoomWidth:550, zoomHeight:400, showTitle: false"> <img src="smallimage.jpg" tppabs="http://metagraphics.eu/spicy/image/smallimage.jpg" alt="" title="Pizza Delicioso"></a> <span class="pricetag"><?php echo $single_product->price;?></span> </div>
          <div class="image-additional">
            <div class="image_car_holder">
              <ul class="jcarousel-skin-opencart">
                <li><a href="" tppabs="http://metagraphics.eu/spicy/image/bigimage00.jpg" class="cloud-zoom-gallery" title="Thumbnail 1" rel="useZoom: 'zoom1', smallImage: 'image/smallimage.jpg' "> <img src="<?php echo base_url().$single_product->pro_image;?>" tppabs="http://metagraphics.eu/spicy/image/tiny1.jpg" alt="Thumbnail 1"> </a></li>
				
				
               
			   
              </ul>
            </div>
            <script type="text/javascript"><!--
      $('.image-additional ul').jcarousel({
	  vertical: false,
	  visible: 4,
	  scroll: 1
      });
      //--></script> 
          </div>
        </div>
        <div class="right">
          <div class="description"> <span>Brand:</span> <a href="#">Apple</a><br>
            <span>Product Code:</span> Product 15<br>
            <span>Reward Points:</span> 100<br>
            <span>Availability:</span> In Stock 
            
            <!-- AddThis Button BEGIN -->
          <div class="addthis_toolbox addthis_default_style "><script type="text/javascript">
      //<![CDATA[
            document.write('<a class="addthis_button_google_plusone" g:plusone:size="medium"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>');
      //]]>
     </script><a class="addthis_button_google_plusone" g:plusone:size="medium"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> 
          </div>
          <script type="text/javascript" src="addthis_widget.js#pubid=ra-4e7280075406aa87" tppabs="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e7280075406aa87"></script> 
          <!-- AddThis Button END -->
            
            <div class="reviews"> <img alt="3 reviews" src="stars-5.png" tppabs="http://metagraphics.eu/spicy/image/stars-5.png">
              <p>Based on <a href="#" title="Read reviews">3 reviews</a></p>
            </div>
          </div>
          <div class="options">
            <div class="option" id="option-217"><b><span class="required">*</span> Select Size:</b>
              <select name="option[217]">
                <option value=""> --- Please Select --- </option>
                <option value="4">Small (+$4.70) </option>
                <option value="3">Middle (+$3.53) </option>
                <option value="1">Large (+$1.18) </option>
              </select>
            </div>
            <div class="option" id="option-223"><b>Add Ingredients:</b>
              <input type="checkbox" id="option-value-9" value="9" name="option[223][]">
              <label for="option-value-9"> Checkbox 2 (+$23.50) </label>
              <br>
              <input type="checkbox" id="option-value-10" value="10" name="option[223][]">
              <label for="option-value-10"> Checkbox 3 (+$35.25) </label>
              <br>
              <input type="checkbox" id="option-value-11" value="11" name="option[223][]">
              <label for="option-value-11"> Checkbox 4 (+$47.00) </label>
              <br>
            </div>
          </div>
<form action="<?php echo base_url();?>Cart/add_to_cart" method="POST"> 
            <div class="cart"> <span class="label">Qty:</span>
            <input type="text" value="1" size="2" name="quantity" id="qty">
            <input type="hidden" name="pro_id"value="<?php echo $single_product->pro_id;?>" size="2" name="quantity" id="qty">
            <button type="submit" id="button-cart" title="Add to Cart"><span>Add to Cart</span></button>
            <a href="#" class="wish_button" title="Add to Wish List">Add to Wish List</a> <a href="#" class="compare_button" title="Add to Compare">Add to Compare</a> </div>
            </form>
          <div class="tags"> <span class="label">Tags:</span> <a href="#">Pizza</a> <a href="#">Italian</a> <a href="#">Food</a> <a href="#">Delivery</a> <a href="#">Vegetarian</a> <a href="#">Sample tag</a> </div>
        </div>
        <div class="clear"></div>
      </div>
      <!-- END OF PRODUCT INFO -->
      
      <div id="content">
        <div class="box">
          <h2 class="heading-title"><span>Description</span></h2>
          <div class="box-content">
            <p><?php echo $single_product->pro_description;?></p>
          </div>
        </div>
        
       
      </div>
    </div>
  </div>