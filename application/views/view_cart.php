<?php
$contents=$this->cart->contents();

?>

<div class="inner">
      <div class="breadcrumb"> <a href="">Home</a> » <a href="account.html">Account</a> » Shopping Cart</div>
      <h2 class="heading-title"><span>Shopping Cart (4.90 kg)</span></h2>
      <div id="content">
        <div class="cart-info">
           
          <table>
            <thead>
              <tr>
                <td class="remove">Remove</td>
                <td class="image">Image</td>
                <td class="name">Product Name</td>
                <td class="quantity">Quantity</td>
                <td class="price">Unit Price</td>
                <td class="total">Total</td>
              
              </tr>
            </thead>
            <tbody>
               <?php
			   foreach($contents as $v_contents){?>
				   
				   
				   
				   	  <tr>
                                  <td class="remove"> <a href="<?php echo base_url();?>cart/delete_cart/<?php echo $v_contents['rowid'];?>">remove </a></td>
                <td class="image"><a href="product.html"><img src="<?php echo base_url().$v_contents['image'];?>" alt="Spicylicious store" width="50"></a></td>
                <td class="name"><a href="product.html"></a> <span class="stock"><?php echo $v_contents['name'];?></span>
                 
                </td>
            <form action="<?php echo base_url();?>cart/update_cart" method="POST">
                <td class="quantity"><input type="text" size="3" value="<?php echo $v_contents['qty'];?>" name="qty"> 
                <button type="submit">  <input type="hidden" name="rowid" value="<?php echo $v_contents['rowid'];?>"> update</button>
                </td>
            </form>
                <td class="price"><?php echo $v_contents['price'];?></td>
                <td class="total"><?php echo $v_contents['subtotal'];?></td>
              </tr> 
			  <?php  }
			   ?>
					
				
					
				
                 
                
             
             
            </tbody>
          </table>
        </div>
        
          </div>
          
        <div class="cart-total">
          <table>
            <tbody>
              <tr>
                <td colspan="5"></td>
                <td class="right"><b>Sub-Total:</b></td>
                <td class="right numbers"><?php echo $this->cart->total();?></td>
              </tr>
              <tr>
                <td colspan="5"></td>
                <td class="right"><b>VAT 10%:</b></td>
                <td class="right numbers">
				<?php $vat=(10*$this->cart->total())/100;
				
				echo $vat;
				?> </td>
              </tr>
              <tr>
                <td colspan="5"></td>
                <td class="right numbers_total"><b> Grand Total:</b></td>
                <td class="right numbers_total"><?php echo $this->cart->total()+$vat;?></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="buttons">
          <div class="left"><a class="button" onclick="#"><span>Update</span></a></div>
          <div class="right"><a class="button" href="#"><span>Checkout</span></a></div>
          <div class="center"><a class="button" href="<?php echo base_url();?>welcome"><span>Continue Shopping</span></a></div>
        </div>
      </div>
    </div>